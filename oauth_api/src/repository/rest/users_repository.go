package rest

import (
	"encoding/json"
	"time"

	httprest "github.com/mercadolibre/golang-restclient/rest"
	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/loggers"
	errors "github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/rest_errors"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/domain/users"
)

var userRestClient = httprest.RequestBuilder{
	BaseURL: "http://127.0.0.1:9000", //localhost???
	Timeout: 3000 * time.Millisecond,
}

type RestUsersRepo interface {
	LoginUser(string, string) (*users.User, *errors.RestErr)
}

type usersRepository struct {
}

func NewRepository() RestUsersRepo {
	return &usersRepository{}
}

func (r *usersRepository) LoginUser(email, password string) (*users.User, *errors.RestErr) {
	request := users.LoginRequest{
		Email:    email,
		Password: password,
	}

	//	isActive := userRestClient.Get("/ping")
	//	loggers.Info(fmt.Sprintln("Accessing UserAPI. Received: ", isActive))
	//	if isActive != nil {
	//		return nil, errors.NewInternalServerError("UserAPI micro-service is down or unreachable", nil)
	//	}

	response := userRestClient.Post("/users/login", request)
	//loggers.Info(fmt.Sprintln("Accessing UserAPI. Received: ", response))

	// Timeout checks - received nil data
	if response == nil || response.Response == nil {
		if response == nil {
			loggers.Info("response is nil")
		}
		if response.Response == nil {
			loggers.Info("response.Response is nil")
		}
		return nil, errors.NewInternalServerError("Invalid client response when trying to login", nil)
	}

	// Manage errors during the creation processs
	if response.StatusCode > 299 {
		var restErr errors.RestErr
		err := json.Unmarshal(response.Bytes(), &restErr)
		if err != nil {
			return nil, errors.NewInternalServerError("Login error: invalid error interface", err)
		}
		return nil, &restErr
	}

	// Convert resonse into user
	var user users.User
	if err := json.Unmarshal(response.Bytes(), &user); err != nil {
		return nil, errors.NewInternalServerError("Error when trying to unmarshall response", err)
	}

	return &user, nil
}
