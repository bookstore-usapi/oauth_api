package db

import (
	"fmt"

	"github.com/gocql/gocql"
	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/loggers"
	errors "github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/rest_errors"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/clients/cassandra"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/domain/access_token"
)

const (
	queryGetToken      = "SELECT access_token, user_id, client_id, expires FROM access_tokens WHERE access_token=?;"
	queryCreateToken   = "INSERT INTO access_tokens(access_token, user_id, client_id, expires) VALUES (?, ?, ?, ?);"
	queryUpdateExpires = "UPDATE access_tokens SET expires=? WHERE access_token=?;"
)

func NewRepository() DbRepository {
	return &dbRepository{}
}

type DbRepository interface {
	GetById(string) (*access_token.AccessToken, *errors.RestErr)
	Create(access_token.AccessToken) *errors.RestErr
	UpdateExpirationTime(access_token.AccessToken) *errors.RestErr
}

type dbRepository struct {
}

func (r *dbRepository) GetById(id string) (*access_token.AccessToken, *errors.RestErr) {
	var result access_token.AccessToken
	if cqlErr := cassandra.GetSession().Query(queryGetToken, id).Scan(
		&result.AccessToken,
		&result.UserId,
		&result.ClientId,
		&result.Expires,
	); cqlErr != nil {
		if cqlErr == gocql.ErrNotFound {
			loggers.Error("No access token with this ID", cqlErr)
			return nil, errors.NewNotFoundError("No access token with this ID")
		}
		loggers.Error("Unhandled DB scan error", cqlErr)
		return nil, errors.NewInternalServerError("DB Error", cqlErr)
	}
	return &result, nil
}

func (r *dbRepository) Create(at access_token.AccessToken) *errors.RestErr {
	if cqlErr := cassandra.GetSession().Query(queryCreateToken,
		at.AccessToken,
		at.UserId,
		at.ClientId,
		at.Expires,
	).Exec(); cqlErr != nil {
		loggers.Error("DB create error", cqlErr)
		return errors.NewInternalServerError(fmt.Sprintln("DB  Error:", cqlErr.Error()), cqlErr)
	}
	return nil
}

func (r *dbRepository) UpdateExpirationTime(at access_token.AccessToken) *errors.RestErr {
	if cqlErr := cassandra.GetSession().Query(queryUpdateExpires,
		at.Expires,
		at.AccessToken,
	).Exec(); cqlErr != nil {
		return errors.NewInternalServerError(cqlErr.Error(), cqlErr)
	}

	return nil
}
