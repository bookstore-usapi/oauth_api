package access_tokens

import (
	"strings"

	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/loggers"
	errors "github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/rest_errors"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/domain/access_token"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/repository/db"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/repository/rest"
)

type Service interface {
	GetById(string) (*access_token.AccessToken, *errors.RestErr)
	Create(access_token.AccessTokenRequest) (*access_token.AccessToken, *errors.RestErr)
	UpdateExpirationTime(access_token.AccessToken) *errors.RestErr
}

type service struct {
	restUsersRepo rest.RestUsersRepo
	dbRepo        db.DbRepository
}

func NewService(usersRepo rest.RestUsersRepo, repo db.DbRepository) Service {
	return &service{
		restUsersRepo: usersRepo,
		dbRepo:        repo,
	}
}

func (s *service) GetById(accessTokenId string) (*access_token.AccessToken, *errors.RestErr) {
	accessTokenId = strings.TrimSpace(accessTokenId)
	if len(accessTokenId) == 0 {
		return nil, errors.NewBadRequestError("Token is empty")
	}

	accessToken, err := s.dbRepo.GetById(accessTokenId)
	if err != nil {
		return nil, err
	}

	return accessToken, nil
}

func (s *service) Create(request access_token.AccessTokenRequest) (*access_token.AccessToken, *errors.RestErr) {

	if err := request.Validate(); err != nil {
		return nil, err
	}

	user, err := s.restUsersRepo.LoginUser(request.Username, request.Password)
	if err != nil {
		return nil, err
	}

	at := access_token.GetNewAccessToken(user.Id)
	at.Generate()

	if err := s.dbRepo.Create(at); err != nil {
		loggers.Info("Error: service failed in DB call")
		return nil, err
	}
	return &at, nil
}

func (s *service) UpdateExpirationTime(at access_token.AccessToken) *errors.RestErr {
	if err := at.Validate(); err != nil {
		return err
	}
	return s.dbRepo.UpdateExpirationTime(at)
}
