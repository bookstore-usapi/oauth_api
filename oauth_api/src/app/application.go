package app

import (
	"github.com/gin-gonic/gin"

	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/clients/cassandra"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/http"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/repository/db"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/repository/rest"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/services/access_tokens"
)

var router = gin.Default()

func StartApplication() {

	// Check DB is online
	session := cassandra.GetSession()
	session.Close()

	atHandler := http.NewAccessTokenHandler(access_tokens.NewService(rest.NewRepository(), db.NewRepository()))

	router.GET("/oauth/access_token/:access_token_id", atHandler.GetById)
	router.POST("/oauth/access_token", atHandler.CreateToken)

	router.Run(":9001")
}
