package main

import (
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/app"
)

func main() {
	app.StartApplication()
}

