package access_token

import (
	"testing"
	"time"
)

func TestGetNewAccessToken(t *testing.T) {
	at := GetNewAccessToken(0)
	if at.IsExpired() {
		t.Error("New token shouldn't be nil")
	}

	if at.AccessToken != "" {
		t.Error("New token shouldn't have an access ID")
	}

	if at.UserId != 0 {
		t.Error("New token shouldn't have an associated user")
	}
}

func TestAccessTokenIsExpired(t *testing.T) {
	at := AccessToken{}
	if !at.IsExpired() {
		t.Error("Empty token should be expired by default")
	}
	at.Expires = time.Now().UTC().Add(3 * time.Hour).Unix()
	if at.IsExpired() {
		t.Error("Access token shouldn't be expired")
	}
}
