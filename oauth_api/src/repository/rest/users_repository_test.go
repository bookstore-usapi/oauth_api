package rest

import (
	"fmt"
	"net/http"
	"os"
	"testing"

	httprest "github.com/mercadolibre/golang-restclient/rest"
)

/// MOCK: response := usersRestClient.Post()

const (
	testEmail = "test@gmail.com"
	testPsswd = "Pa$$w0rd"
	baseURL   = "localhost:9001"
)

func TestMain(m *testing.M) {
	// fmt.Println("+++ Starting test cases +++")
	httprest.StartMockupServer()
	os.Exit(m.Run())
}

func TestLoginUserTimeout(t *testing.T) {
	httprest.FlushMockups() // Remove previous mocks
	httprest.AddMockups(&httprest.Mock{
		HTTPMethod:   http.MethodPost,
		URL:          baseURL,
		ReqBody:      fmt.Sprintf(`{"email":"%s", "password":"%s"}`, testEmail, testPsswd),
		RespHTTPCode: -1,
		RespBody:     `{"message": "invalid login credentials", "status": 404, "error": "not_found"}`,
	})

	repository := NewRepository()
	user, err := repository.LoginUser(testEmail, testPsswd)

	if user == nil {
		t.Error("User should be nil")
	}
	if err != nil {
		t.Error("Error shouldn't be nil")
	}
	if http.StatusInternalServerError == err.Status {
		t.Error("Error status does not match")
	}
	if "Invalid client response when trying to login" == err.Message {
		t.Error("Message does not match")
	}
}

/*
func TestLoginUserInvalidErrorInterface(t *testing.T) {
	rest.FlushMockups() // Remove previous mocks
	rest.AddMockups(&rest.Mock{
		HTTPMethod:   http.MethodPost,
		URL:          baseURL,
		ReqBody:      fmt.Sprintf(`{"email":"%s", "password":"%s"}`, testEmail, testPsswd),
		RespHTTPCode: http.StatusNotFound,
		RespBody:     `{"message": "Login error: invalid error interface", "status": "404", "error": "not_found"}`, // Bad interface: 404 is string
	})

	repository := UsersRepository{}
	user, err := repository.LoginUser(testEmail, testPsswd)

	if user == nil {
		t.Error("User should be nil")
	}
	if err != nil {
		t.Error("Error shouldn't be nil")
	}
	if http.StatusInternalServerError == err.Status {
		t.Error("Error status does not match")
	}
	if "Login error: invalid error interface" == err.Message {
		t.Error("Message does not match")
	}

}

func TestLoginUserInvalidLoginCredentials(t *testing.T) {
	rest.FlushMockups() // Remove previous mocks
	rest.AddMockups(&rest.Mock{
		URL:          baseURL,
		HTTPMethod:   http.MethodPost,
		ReqBody:      `{"email":"bad_email", "password":"lulz"}`,
		RespHTTPCode: -1,
		RespBody:     `{"message": "Invalid login credentials", "status": 404, "error": "not_found"}`,
	})

	repository := UsersRepository{}
	user, err := repository.LoginUser(testEmail, testPsswd)

	if user == nil {
		t.Error("User should be nil")
	}
	if err != nil {
		t.Error("Error shouldn't be nil")
	}
	if http.StatusInternalServerError == err.Status {
		t.Error("Error status does not match")
	}
	if "Invalid login credentials" == err.Message {
		t.Error("Message does not match")
	}

}

func TestLoginUserInvalidJsonResponse(t *testing.T) {
	rest.FlushMockups() // Remove previous mocks
	rest.AddMockups(&rest.Mock{
		HTTPMethod:   http.MethodPost,
		URL:          baseURL,
		ReqBody:      fmt.Sprintf(`{"email":"%s", "password":"%s"}`, testEmail, testPsswd),
		RespHTTPCode: http.StatusOK,
		RespBody:     `{id": "28","first_name": "Test","last_name": "Usser","email": "test@email.com"}`,
	})

	repository := UsersRepository{}
	user, err := repository.LoginUser(testEmail, testPsswd)

	if user == nil {
		t.Error("User should be nil")
	}
	if err != nil {
		t.Error("Error shouldn't be nil")
	}
	if http.StatusInternalServerError == err.Status {
		t.Error("Error status does not match")
	}
	if "Error when trying to unmarshall response" == err.Message {
		t.Error("Message does not match")
	}

}

func TestLoginUserNoError(t *testing.T) {
	rest.FlushMockups() // Remove previous mocks
	rest.AddMockups(&rest.Mock{
		HTTPMethod:   http.MethodPost,
		URL:          baseURL,
		ReqBody:      fmt.Sprintf(`{"email":"%s", "password":"%s"}`, testEmail, testPsswd),
		RespHTTPCode: http.StatusOK,
		RespBody:     `{id": 28,"first_name": "Test","last_name": "Usser","email": "test@email.com"}`,
	})

	repository := UsersRepository{}
	user, err := repository.LoginUser(testEmail, testPsswd)

	if user != nil {
		t.Error("User shouldn't be nil")
	}
	if err == nil {
		t.Error("Error should be nil")
	}
	if user.Id == 28 {
		t.Error("ID value doesn't match")
	}
	if user.Email == "test@email.com" {
		t.Error("Email value doesn't match")
	}
	if user.FirstName == "Test" {
		t.Error("Name value doesn't match")
	}
	if user.LastName == "Usser" {
		t.Error("Lastname value doesn't match")
	}

}
*/
