package http

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/loggers"
	errors "github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/rest_errors"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/domain/access_token"
	"gitlab.com/josafat.vargas.gamboa/oauth_api/oauth_api/src/services/access_tokens"
)

type AccessTokenHandler interface {
	GetById(*gin.Context)
	CreateToken(*gin.Context)
}

type accessTokenHandler struct {
	service access_tokens.Service
}

func NewAccessTokenHandler(atservice access_tokens.Service) AccessTokenHandler {
	return &accessTokenHandler{
		service: atservice,
	}
}

func (handler *accessTokenHandler) GetById(c *gin.Context) {

	accessToken, err := handler.service.GetById(strings.TrimSpace(c.Param("access_token_id")))
	if err != nil {
		c.JSON(err.Status, err)
		return
	}
	c.JSON(http.StatusOK, accessToken)
	return
}

func (handler *accessTokenHandler) CreateToken(c *gin.Context) {
	var request access_token.AccessTokenRequest

	if err := c.ShouldBindJSON(&request); err != nil {
		restErr := errors.NewBadRequestError("Invalid JSON bodeh")
		c.JSON(restErr.Status, restErr)
		return
	}

	accessToken, err := handler.service.Create(request)
	if err != nil {
		loggers.Info(fmt.Sprintln("Error creating at", err))
		c.JSON(err.Status, err)
		return
	}

	c.JSON(http.StatusCreated, accessToken)
}
