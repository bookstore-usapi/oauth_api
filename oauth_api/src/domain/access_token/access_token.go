package access_token

import (
	"fmt"
	"strings"
	"time"

	"github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/cryptos"
	errors "github.kyndryl.net/Josafat-Vargas-Gamboa/Bookstore-utils-go/rest_errors"
)

const (
	expirationTime       = 24
	grantTypePassword    = "password"
	grantTypeClientCreds = "client_credentials"
)

type AccessTokenRequest struct {
	GrantType string `json:"grant_type"`
	Scope     string `json:"scope"`
	// for Password grant type
	Username string `json:"username"`
	Password string `json:"password"`
	// for Client_credentials grant type
	ClientId     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

func (at *AccessTokenRequest) Validate() *errors.RestErr {
	switch at.GrantType {
	case grantTypePassword:
		break
	case grantTypeClientCreds:
		break
	default:
		return errors.NewBadRequestError("Invalid grant type parameter")
	}
	return nil
}

type AccessToken struct {
	AccessToken string `json:"access_token"`
	UserId      int64  `json:"user_id"`
	ClientId    int64  `json:"client_id"`
	Expires     int64  `json:"expires"`
}

func (at *AccessToken) Validate() *errors.RestErr {
	at.AccessToken = strings.TrimSpace(at.AccessToken)
	if at.AccessToken == "" {
		return errors.NewBadRequestError("Invalid Access Token ID")
	}
	if at.UserId <= 0 {
		return errors.NewBadRequestError("Invalid User Id ")
	}
	if at.ClientId <= 0 {
		return errors.NewBadRequestError("Invalid Client Id")
	}
	if at.Expires <= 0 {
		return errors.NewBadRequestError("Invalid expiration time")
	}
	return nil
}

func GetNewAccessToken(user_id int64) AccessToken {
	return AccessToken{
		UserId:  user_id,
		Expires: time.Now().UTC().Add(expirationTime * time.Hour).Unix(),
	}
}

func (at AccessToken) IsExpired() bool {
	return time.Unix(at.Expires, 0).Before(time.Now().UTC())
}

func (at *AccessToken) Generate() {
	at.AccessToken = cryptos.GetMd5(fmt.Sprintf("at-%d-%d-ran", at.UserId, at.Expires))
}
